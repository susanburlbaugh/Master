import java.net.URI;
import java.net.URL;

import org.jetel.component.AbstractGenericTransform;
import org.jetel.component.fileoperation.pool.ConnectionPool;
import org.jetel.component.fileoperation.pool.PooledS3Connection;
import org.jetel.component.fileoperation.pool.S3Authority;
import org.jetel.data.DataField;
import org.jetel.data.DataRecord;
import org.jetel.exception.ComponentNotReadyException;
import org.jetel.exception.ConfigurationStatus;
import org.jetel.util.file.FileUtils;
import org.jetel.util.protocols.amazon.S3Utils;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * This is an example custom reader. It shows how you can create records using a data source.
 */
public class S3ListFiles extends AbstractGenericTransform {

	private static final String FILE_URL_PROPERTY = "fileUrl";
	private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();

	@Override
	public void execute() throws Exception {
		String fileUrl = getProperties().getStringProperty(FILE_URL_PROPERTY);
		
		/** Output record prepared for writing to output port 0. */
		DataRecord record = outRecords[0];
		DataField keyField = record.getField("key");
		DataField lastModifiedField = record.getField("lastModified");

		URL url = FileUtils.getFileURL(fileUrl);
		URI uri = url.toURI();
		S3Authority authority = new S3Authority(uri);
		PooledS3Connection connection = null;
		try {
			connection = (PooledS3Connection) CONNECTION_POOL.borrowObject(authority);
			AmazonS3 s3client = connection.getService();
			String[] path = S3Utils.getPath(uri);
			System.out.println("KLS path = " + path);
			final String bucketName = path[0];
			final String prefix = FileUtils.appendSlash(path[1]);
			final int prefixLength = prefix.length();
			ListObjectsRequest request = S3Utils.listObjectRequest(bucketName, prefix, "/");
			ObjectListing listing = s3client.listObjects(request);
			boolean truncated;
			do {
				truncated = listing.isTruncated();
				
				for (S3ObjectSummary object: listing.getObjectSummaries()) {
					String key = object.getKey();
					if (key.length() > prefixLength) { // skip the parent directory itself
						keyField.setValue(object.getKey());
						lastModifiedField.setValue(object.getLastModified().getTime());
						writeRecordToPort(0, record);
						record.reset();
					}
				}
				
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				
				if (truncated) {
					listing = s3client.listNextBatchOfObjects(listing);
				}
			} while (truncated);
		} finally {
			if (connection != null) {
				connection.returnToPool();
			}
		}
	}

	@Override
	public ConfigurationStatus checkConfig(ConfigurationStatus status) {
		super.checkConfig(status);

		/** This way you can check connected edges and their metadata. */
		/*
		if (getComponent().getOutPorts().size() < 1) {
			status.add("Output port must be connected!", Severity.ERROR, getComponent(), Priority.NORMAL);
			return status;
		}

		DataRecordMetadata outMetadata = getComponent().getOutputPort(0).getMetadata();
		if (outMetadata == null) {
			status.add("Metadata on output port not specified!", Severity.ERROR, getComponent(), Priority.NORMAL);
			return status;
		}

		if (outMetadata.getFieldPosition("myMetadataFieldName") == -1) {
			status.add("Incompatible output metadata!", Severity.ERROR, getComponent(), Priority.NORMAL);
		}
		*/
		return status;
	}

	@Override
	public void init() {
		super.init();
	}

	@Override
	public void preExecute() throws ComponentNotReadyException {
		super.preExecute();
	}

	@Override
	public void postExecute() throws ComponentNotReadyException {
		super.postExecute();
	}
}
